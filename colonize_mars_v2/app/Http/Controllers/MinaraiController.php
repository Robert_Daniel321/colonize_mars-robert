<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Minarai;

class MinaraiController extends Controller
{
    public function index()
    {
        $minerais = Minarai::all();
        return view('Minerais.index', compact('minerais'));
    }

    public function create()
    {
        return view('Minerais.create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nom' => 'required',
            'description' => 'required',
        ]);

        Minarai::create($validatedData);

        return redirect('/minerais')
            ->with('success', 'Minerai créé avec succès.');
    }

    public function edit($id)
    {
        $minerais = Minarai::findOrFail($id);

        return view('Minerais.edit', compact('minerais'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'nom' => 'required',
            'description' => 'required',
        ]);

        $minerai = Minarai::findOrFail($id);
        $minerai->update($validatedData);

        return redirect()->route('minerais.index')
            ->with('success', 'Minerai modifié avec succès.');
    }

    public function destroy($id)
    {
        $minerai = Minarai::findOrFail($id);
        $minerai->delete();

        return redirect('/minerais')
            ->with('success', 'Minerai supprimé avec succès.');
    }
}
