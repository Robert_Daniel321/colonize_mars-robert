<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Zone;
use App\Models\Minarai;

class ZoneController extends Controller
{
    public function index()
    {
        $zones = Zone::all();

        return view('Zone.index', compact('zones'));
    }

    public function create()
    {
        $minerais = Minarai::all();

        return view('Zone.create', compact('minerais'));
    }

    public function store(Request $request)
    {
        $zone = new Zone();
        $zone->latitude = $request->latitude;
        $zone->longitude = $request->longitude;
        $zone->dangerosite = $request->dangerosite;
        $zone->date = $request->date;
        $zone->save();
        // $zone->minerais()->sync($request->minerai);

        return redirect('/zone');
    }

    public function edit($id)
    {
        $zone = Zone::findOrFail($id);
        $minerais = Minarai::all();

        return view('Zone.edit', compact('zone', 'minerais'));
    }

    public function update(Request $request, $id)
    {
        $zone = Zone::findOrFail($id);
        $zone->latitude = $request->latitude;
        $zone->longitude = $request->longitude;
        $zone->dangerosite = $request->dangerosite;
        $zone->date = $request->date;
        $zone->save();
        $zone->minerais()->sync($request->minerai);

        return redirect('/zone');
    }

    public function destroy($id)
    {
        $zone = Zone::findOrFail($id);
        $zone->delete();

        return redirect('/zone');
    }
}

