<?php

namespace App\Models;

use App\Models\Zone;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Minarai extends Model
{
    use HasFactory;

    protected $fillable = [
        'nom',
        'description',
    ];

    public function zones()
    {
        return $this->belongsToMany(Zone::class);
    }
}

