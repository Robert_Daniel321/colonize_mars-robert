<?php

namespace App\Models;

use App\Models\Minarai;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Zone extends Model
{
    use HasFactory;

    protected $fillable = [
        'latitude',
        'longitude',
        'dangerosite',
        'date',
    ];

    public function minerais()
    {
        return $this->belongsToMany(Minarai::class);
    }
}

