@extends('layouts.app')

@section('content')
<div class="container">
    <h1>Modifier {{$minerais->nom}}</h1>
    <form action="{{ route('minerais.update', $minerais->id) }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nom">Nom:</label>
            <input type="text" class="form-control" name="nom" value="{{ $minerais->nom }}" required/>
        </div>

        <div class="form-group">
            <label for="description">Description:</label>
            <textarea class="form-control" name="description" required>{{ $minerais->description }}</textarea>
        </div>


        <button type="submit" class="btn btn-primary">Modifier</button>
    </form>
</div>
@endsection
