@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Liste des minerais') }}</div>

                    <div class="card-body">
                        <div class="mb-3">
                            <a href="{{ route('minerais.create') }}" class="btn btn-primary">{{ __('Créer un nouveau minerai') }}</a>
                        </div>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">{{ __('Nom') }}</th>
                                    <th scope="col">{{ __('Description') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($minerais as $minerai)
                                    <tr>
                                        <td>{{ $minerai->nom }}</td>
                                        <td>{{ $minerai->description }}</td>
                                        <td>
                                            <a href="{{ route('minerais.edit', $minerai->id) }}" class="btn btn-secondary">{{ __('Modifier') }}</a>
                                            <form action="{{ route('minerais.destroy', $minerai->id) }}" method="POST" style="display: inline-block;">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger" >{{ __('Supprimer') }}</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
