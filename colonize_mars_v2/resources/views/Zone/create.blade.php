@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Ajouter une zone</h1>
        <form method="POST" action="{{ route('zone.store') }}">
            @csrf
            <div class="form-group">
                <label for="latitude">Latitude</label>
                <input type="text" class="form-control" id="latitude" name="latitude" required>
            </div>
            <div class="form-group">
                <label for="longitude">Longitude</label>
                <input type="text" class="form-control" id="longitude" name="longitude" required>
            </div>
            <div class="form-group">
                <label for="dangerosite">Dangérosité</label>
                <input type="number" class="form-control" id="dangerosite" name="dangerosite" required>
            </div>
            <div class="form-group">
                <label for="date">Date de découverte</label>
                <input type="date" class="form-control" id="date" name="date" required>
            </div>
            <!-- <div class="form-group">
                <label for="minerai">Minerai présent</label>
                <select class="form-control" id="minerai" name="minerai[]" multiple>
                    @foreach($minerais as $minerai)
                        <option value="{{ $minerai->id }}">{{ $minerai->nom }}</option>
                    @endforeach
                </select>
            </div> -->
            <button type="submit" class="btn btn-primary">Ajouter</button>
        </form>
    </div>
@endsection
