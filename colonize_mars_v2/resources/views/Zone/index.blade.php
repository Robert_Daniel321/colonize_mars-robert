
   @extends('layouts.app')
   @section('content')
   
   <h1>Liste des zones</h1>
    <a href="{{ route('zone.create') }}">Ajouter une zone</a>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Latitude</th>
                <th>Longitude</th>
                <th>Niveau de dangerosité</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($zones as $zone)
                <tr>
                    <td>{{ $zone->id }}</td>
                    <td>{{ $zone->latitude }}</td>
                    <td>{{ $zone->longitude }}</td>
                    <td>{{ $zone->dangerosite }}</td>
                    <td>{{ $zone->date }}</td>
                    <td>
                        <a href="{{ route('zone.edit', $zone->id) }}" class="btn btn-primary m-3">Modifier</a>
                        <form method="POST" action="{{ route('zone.destroy', $zone->id) }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Supprimer</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@endsection