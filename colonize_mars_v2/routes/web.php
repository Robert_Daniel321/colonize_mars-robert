<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ZoneController;
use App\Http\Controllers\MinaraiController;

// Route::get('/', function () {
//     // return view('welcome');
// });

// Routes pour les zones
Route::get('/zone', [ZoneController::class, 'index'])->name('zone.index');
Route::get('/zone/create', [ZoneController::class, 'create'])->name('zone.create');
Route::post('/zone', [ZoneController::class, 'store'])->name('zone.store');
Route::get('/zone/{id}', [ZoneController::class, 'show'])->name('zone.show');
Route::get('/zone/{id}/edit', [ZoneController::class, 'edit'])->name('zone.edit');
Route::put('/zone/{id}', [ZoneController::class, 'update'])->name('zone.update');
Route::delete('/zone/{id}', [ZoneController::class, 'destroy'])->name('zone.destroy');

// Routes pour les minerais
Route::get('/minerais', [MinaraiController::class, 'index'])->name('minerais.index');
Route::get('/minerais/create', [MinaraiController::class, 'create'])->name('minerais.create');
Route::post('/minerais', [MinaraiController::class, 'store'])->name('minerais.store');
Route::get('/minerais/{id}', [MinaraiController::class, 'show'])->name('minerais.show');
Route::get('/minerais/{id}/edit', [MinaraiController::class, 'edit'])->name('minerais.edit');
Route::put('/minerais/{id}', [MinaraiController::class, 'update'])->name('minerais.update');
Route::delete('/minerais/{id}', [MinaraiController::class, 'destroy'])->name('minerais.destroy');

